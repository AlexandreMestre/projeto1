# Gerador de Notas


ETb = 23.5 
BMb = 141645320.0
IPCb =  2.98
L1Mb = 1171462317.0
BCb = 2131182039.0

	

file = open("perf.txt", "r+").readlines()


for i in xrange(len(file)):
	file[i] = file[i].replace(',', '')
	file[i] = file[i].replace('%', '')
	file[i] = file[i].replace('.', '')
	file[i] = file[i].replace('(', '')
	file[i] = file[i].replace(')', '')
	file[i] = file[i].split(' ')



branch_misses = float(file[4][7])
desvio_branch_misses = (float(file[4][63])/100)
print "Branch Misses:", branch_misses 
print "Desvio Padrao de branch misses: ", desvio_branch_misses,'%', '\n'


bus_cycles = float(file[3][5])
desvio_bus_cycles = (float(file[3][64])/100) 	
print "Numero de Bus-Cycles: ", bus_cycles 
print "Desvio Padrao de Bus-Cycles: ", desvio_bus_cycles,'%', '\n'

icp = (float(file[8][25])/100)
print "IPC do processador: ", icp, '\n'

cacheL1_misses = float(file[10][5])
desvio_cache_misses = (float(file[10][29])/100)
print "Numero de Cache L1 Misses: ", cacheL1_misses 
print "Desvio Padrao do Caches L1 misses", desvio_cache_misses,'%', '\n'


elapsed_time = (float(file[14][6])/1000000000)
desvio_elapsed_time = (float(file[14][54])/100)

print "Tempo de Execucao: ", elapsed_time 
print "Desvio Padrao do tempo de execucao: ", desvio_elapsed_time,'%', '\n'



p1 = (0.4)*(ETb/elapsed_time)
p2 = (0.1)*(BMb/branch_misses)
p3 = (0.2)*(icp/IPCb)
p4 = (0.1)*(L1Mb/cacheL1_misses)
p5 = (0.2)*(BCb/bus_cycles)

NOTA = (p1 + p2 + p3 + p4 + p5)*50

print "NOTA PARA O DESEMPENHO: ", NOTA
