# gnuplot - Grupo 1  

## O que faz?
Ferramenta de linha de comando para criação de gráficos 2D e 3D a partir de funções definidas pelo usuário.

## Por que é bom para medir desempenho?
Por se tratar de um programa que deve analisar uma função e posteriormente gerar um gráfico, é necessário acesso à memória, tanto principal quanto disco, processamento gráfico e de CPU. Assim, pode ser utilizado para medir desempenho de forma geral.

## O que baixar
Para executar o *benchmark* é necessário o *gnuplot* e a ferramenta de análise *perf*.

### gnuplot
Basta executar o seguinte comando para baixar e descompactar o gnuplot:

```
wget https://sourceforge.net/projects/gnuplot/files/gnuplot/5.0.5/gnuplot-5.0.5.tar.gz  
cd gnuplot-5.0.5.tar.gz  
tar -xzvf gnuplot-5.0.5.tar.gz  
```

### perf
A ferramenta faz parte do pacote `linux-tools` e pode ser instalada usando `sudo apt-get install linux-tools`.  
Porém, caso a versão do *Kernel* não seja a mais recente é necessário especificar a versão. Por exemplo: `sudo apt-get install linux-tools-3.2.0.43`.  
Para descobrir a versão do *Kernel* basta executar o comando `uname -r`.

## Como compilar/instalar

### gnuplot
Para que o programa funcione perfeitamente em qualquer computador Linux, devemos instalá-lo em uma pasta local. Assim, dentro da pasta `gnuplot-5.0.5` , basta executar os seguintes comando para instalá-lo na pasta `HOME/gnuplot`:

```
./configure --prefix=$HOME/gnuplot --with-readline=gnu  
make  
make install-strip
```

O *download* e instalação do gnuplot são realizadas automaticamente executando o *script*  `script-install`, presente na pasta `scripts`.

## Como executar
Para executar o programa, primeiramente foram criados dois datasets, contendo, cada um, um milhão de pares ordenados (X,Y). Posteriormente, foi gerado um script que se encontra na pasta `scripts` e executa o gnuplot, contruindo um gráfico 2D a partir do primeiro dataset e gráficos tridimensionais a partir do segundo dataset. Para os gráficos 3D, a coordenada Z foi obtida a patir de funções aplicadas sobre os valores das coordenadas X e Y.  
  
  
Foi criado um Makefile que contém uma regra `install` para instalação das ferramentas e execução dos *scripts*, coletando via *perf* os dados necessários para a análise de desempenho e dando uma nota para a máquina. Desta forma, para executar o *benchmark*, basta utilizar o comando `make install` dentro da pasta `scripts`.  
Caso as ferramentas já tenham sido instaladas, é possível executar o *benchmark* com o comando `make run	`.

## Como medir o desempenho
Para medir o desempenho, utilizou-se a ferramenta perf para coletar os dados necessários. Entre estes dados, estão: *elapsed time*, *branch misses*, *instruction per cycle*, *L1 cache misses* e *bus cycles*.  
O ***elapsed time*** consiste no tempo total necessário para executar o *script* e construir todos os gráficos, refletindo, assim, a velocidade do processador e o tempo de acesso a memória.    
O ***branch misses*** é a quantidade de saltos preditos erroneamente pelo *branch predictor*, dessa forma, permite analisar a qualidade do *branch predictor* do processador.  
O ***instruction per cycle*** consiste na quantidade de instruções executadas por ciclo de clock, consequentemente, quanto maior for seu valor, provavelmente mais rápido será o procesador, dependendo, apenas, da duração de um ciclo.  
O ***L1 cache misses***, por sua vez, mede a quantidade de miss que ocorreram na cache L1, a cache mais próxima do processador.  
E, por fim, ***bus cycles*** é a quantidade de ciclos gastos aguardando por operações de entrada e saída.
Além dessas informações, outros dados podem ser coletados com a ferramenta *perf*, porém são irrelevantes ou redundantes para a análise que se deseja fazer a partir do *benchmark* e, por isso, não foram utilizados.

## Como apresentar o desempenho
O resultado de cada um dos 5 eventos medidos pelo *benchmark* será a média de 5 análises via *perf*, seguido de seu respectivo desvio padrão.  
Além disso, é apresentado uma nota final `N`, calculada a partir da fórmula:  
  
`N = (0.4*ET/ET' + 0.1*BM/BM' + 0.2*IPC'/IPC + 0.1*L1M/L1M' + 0.2*BC/BC') * 50`

As variáveis primas (`X'`) são relativas à máquina sendo análisada, enquanto as demais são relativas à máquina base.  
> ET  - elapsed-time  
> BM  - branch-misses  
> IPC - instructions-per-cycle  
> L1M - L1-cache-misses  
> BC - bus-cycles  

## Medições base (uma máquina)

A máquina utiliza foi: Intel® Core™ i5-4590 CPU @ 3.30GHz L1: 32K, L2: 256K, L3: 6M, 8GB DDR3, SSD 512gb   

> ET = 23.5 ± 0.46%  
> BM = 1411645532 ± 0.41%  
> IPC = 2.98 ± 0.01%  
> L1M = 1171462317 ± 0.08%  
> BC = 2121182039 ± 0.45%  